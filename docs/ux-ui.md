---
id: ux-ui
title: UX/UI
sidebar_label: UX/UI
---

## UX/UI ontwerp variant 2

Voor het geaggregeerde schuldinformatie-overzicht is een UX/UI ontwerp gemaakt dat gericht is op gemakkelijk en veilig gebruik.

Noot: _de mobiele app is gebouwd met behulp van Flutter, en momenteel enkel beschikbaar in testversie op iOS na contact met team Blauwe Knop bij VNG Realisatie._

### UI ontwerp happy flow

![App installeren](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/1-1-app-installeren.png)

![Startscherm](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/1-2-startscherm.png)

![Kies organisaties 1](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/1-3-kies-organisaties-1.png)

![Kies organisaties 2](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/1-5-kies-organisaties-2-OPTIONEEL.png)

![Kies organisaties 3](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/1-7-kies-organisaties-3-OPTIONEEL.png)

![Registrator kiezen 1](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-1-aggregator-selecteren-1.png)

![Meer informatie](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-1b-meer-informatie.png)

![Registrator kiezen 2](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-2-aggregator-selecteren-2.png)

![Verzoek klaar voor indienen bij registrator](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-3-verzoek-klaar-voor-indienen-bij-aggregator.png)

![Verzoek indienen bij registrator 1](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-4-verzoek-indienen-bij-aggregator-1.png)

![Inloggen met DigiD bij registrator](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-5-digid-inloggen-bij-aggregator.png)

![Verzoek indienen bij registrator 2](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-6-verzoek-indienen-bij-aggregator-2.png)

![Verzoek bevestigd](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-7-verzoek-bevestigd.png)

![Terug naar de app](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/2-8-terug-naar-de-BK-app.png)

![Verzoek ingediend](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-1-verzoek-ingediend.png)

![Schuldinformatie opgehaald](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-2-schuldinformatie.png)

![Detail schuldinformatie per organisatie](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-3-schuldinformatie-detail-per-organisatie.png)

### Overige UI ontwerpen

![Melding: organisatie is traag](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-4-MELDING-organisatie-is-traag.png)

![Melding: koppeling verlopen](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-5-MELDING-koppeling-werkt-niet-of-verlopen.png)

![Melding: koppeling verlopen (detail)](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-6-MELDING-koppeling-werkt-niet-of-verlopen-detail.png)

![|Melding: geen internet](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-7-MELDING-geen-internet.png)

![Vordering overgedragen](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/4-8-vordering-overgedragen.png)

![Veelgestelde vragen](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/5-1-veelgestelde-vragen.png)

![Status per organisatie](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/6-1-meerdere-organisaties-in-meerdere-status.png)
