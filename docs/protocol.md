---
id: protocol
title: Protocol
sidebar_label: Protocol
---

## Protocolbeschrijving

We gaan in deze protocolbeschrijving uit van variant 2 van het procesmodel, waarin de burger een generiek verzoek om schuldinformatie vastlegt bij een organisatie met de registratorrol (bijvoorbeeld de eigen woongemeente).

We beschrijven hier het protocol dat uitgevoerd wordt tussen de burger, diens app, de bronorganisaties en, in variant 2 van het protocol, de registrator.

![](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/varianten.png)

### Veiligheid

Iedere Blauwe Knop app genereert bij het opstarten een public/private keypair. Dit keypair wordt gebruikt om cryptografische bewijzen te leveren aan organisaties waarmee de app contact heeft. Tijdens relevante processen levert de app bewijs van bezit van de private key.

### Variant 2 - Generiek verzoek indienen bij registrator

Dit deel van het protocol bestaat in variant 2 grofweg uit de volgende stappen:

- Verzoek samenstellen in de app (incl public key)
- Verzoek opsturen naar de registrator (in de browser op de telefoon)
- Inloggen bij de registrator (via DigiD)
- Het verzoek bevestigen en indienen bij de registrator
- [optioneel: een sessie starten met de registrator op basis van het public/private keypair in de app]
- Status van het ingediende verzoek ophalen en weergeven aan de gebruiker

![](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/variant_2_step_1_create_debt_request.png)

### Variant 2 - Het generieke verzoek gebruiken om specifieke, afzonderlijke koppelingen (links) te leggen tussen de app en alle bronorganisaties

Dit deel van het protocol bestaat in variant 2 grofweg uit de volgende stappen:

- Status van het ingediende verzoek controleren
- Een sessie starten met de bronorganisatie op basis van het public/private keypair in de app
- Checken of er al een link bestaat tussen de app en bronorganisatie
- Indien deze er nog niet is: een nieuwe link leggen tussen de app en bronorganisatie

![](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/variant_2_step_2_open_app_with_debt_request.png)

### Variant 2 - Schuldinformatie ophalen bij een bronorganisatie op basis van een reeds gelegde link tussen de app en de specifieke bronorganisatie

Dit deel van het protocol bestaat in variant 2 grofweg uit de volgende stappen:

- Status van het ingediende verzoek controleren
- Een sessie starten met de bronorganisatie op basis van het public/private keypair in de app
- Schuldinformatie ophalen bij de bronorganisatie

![](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/variant_2_step_3_get_debt.png)