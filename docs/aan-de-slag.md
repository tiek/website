---
id: aan-de-slag
title: Aan de slag
sidebar_label: Aan de slag
---

_Noot: Deze handleiding gaat uit van variant 2 van het Blauwe Knop protocol: het ophalen van schuldinformatie direct bij de bronnen door het vastleggen van een generiek verzoek bij een registrator_

Onderdeel van het Blauwe Knop Stelsel zijn de volgende rollen:

- Burger/eindgebruiker: de natuurlijke persoon die met de Blauwe Knop Schuldinformatie toepassing zijn schuldinformatie verzamelt
- Bronorganisatie: een organisatie waar schuldinformatie kan worden opgevraagd door de burger
- Registrator: een organisatie waar de burger een generiek verzoek tot het ophalen van schuldinformatie kan indienen (bijvoorbeeld de gemeente waar de burger inwoner is)
- Stelselbeheerder: de partij die de deelnemerslijst beheert van de partijen die meedoen aan het Blauwe Knop Stelsel (voor de proeftuinomgeving is dit VNG Realisatie)

De volgende architectuurplaat (context/container) geeft een schematisch overzicht van de benodigde componenten per rol, en hun interactie:

![C4 architecture](/architecture/c4.png)

## A. Aan de slag als burger

- Installeer de Blauwe Knop Schuldinformatie app op een mobiele telefoon.
- Start de app en doorloop de wizard om een generiek verzoek samen te stellen en in te dienen
- Vervolgens haalt de Blauwe Knop Schuldinformatie app alle schuldinformatie rechtsstreeks op bij alle bronorganisaties.

## B. Aan de slag als bronorganisatie

### 1. Benodigde componenten

Over het algemeen is schuldinformatie bij een bronorganisatie in één of meerdere (al dan niet bestaande) registers opgeslagen.

Voor een bronorganisatie zijn de volgende componenten vereist:

Proceslaag:

- App Debt Process component: Procescomponent voor het ophalen van Schuldinformatie bij een bronorganisatie.
- Session Process component: Procescomponent voor het starten en stoppen van kortdurende Sessies tussen de Blauwe Knop Schuldinformatie app en een bronorganisatie.
- App Link Process component: Procescomponent voor het leggen van langdurige Koppelingen (Links) tussen de Blauwe Knop Schuldinformatie app en een bronorganisatie.

Integratielaag:

- NLX (Outway) component: Via de NLX Outway component wordt veilig en eenvoudig verbinding gemaakt met API's bij andere organisaties in het stelsel (in het bijzonder met Debt Request Register(s) bij registrator(s)).

Service-laag:

- Session Register component: Dataservice (API-)component voor het ontsluiten van Sessies binnen een bronorganisatie.
- Link Register component: Dataservice (API-)component voor het ontsluiten van Koppelingen (Links) binnen een bronorganisatie.
- 1 of meer Debt Registers: één of meer bronsystemen bij de bronorganisatie die de Blauwe Knop Schuldinformatie Data API specificatie [LINK] implementeren.

Datalaag:

- Sessions (Redis store) component: opslag van Sessies
- Link (Redis store) component: opslag van Koppelingen (Links).

Deze componenten dienen bij de bronorganisatie geïnstalleerd te worden, geconfigureerd te worden, en de API's van de procescomponenten dienen publiek beschikbaar te worden gemaakt zodat de Blauwe Knop Schuldenoverzicht app deze proces-API's kan aanspreken.

### 2. Het installeren en configureren van componenten

De componenten zijn beschikbaar via Gitlab. Als Golang broncode/applicaties en in de vorm van docker containers. Deployment bij VNG vindt plaats op Haven compliant infrastructuur mbv [Helm](https://helm.sh).

Op dit moment draait er een aantal demo-organisaties in de testomgeving bij VNG Realisatie. De deployment-configuratie daarvan is ter reference in te zien op <https://gitlab.com/commonground/blauwe-knop/deployment/-/tree/master/organizations>. Een voorbeeld van een test-organisatie met enkel de rol 'bronorganisatie' is <https://gitlab.com/commonground/blauwe-knop/deployment/-/tree/master/organizations/gemeente-coevorden>

Hieronder beschrijven we de benodigde configuratie per component in meer detail.

#### NLX (Outway)

Voor bronorganisaties is een NLX Outway vereist om via het NLX netwerk op veilige en eenvoudige wijze de registrator(s) te kunnen bevragen.

Om een NLX Outway te starten en beheren zijn ook een aantal dependency componenten nodig van NLX. Om verdubbeling in de documentatie te voorkomen gaan we daar hier niet op in, maar verwijzen we naar de NLX documentatie.

Voor informatie over het installeren van de NLX Outway en vereiste depencencies zoals NLX Management, zie [https://docs.nlx.io/try-nlx/docker/introduction](https://docs.nlx.io/try-nlx/docker/introduction) als je NLX wilt installeren via docker of [https://docs.nlx.io/try-nlx/helm/introduction](https://docs.nlx.io/try-nlx/helm/introduction) als je NLX wilt installeren op een Kubernetes cluster

#### App Debt Process component

Het app-debt-process kan geconfigureerd worden met een aantal [environment variables](https://gitlab.com/commonground/blauwe-knop/app-debt-process/-/blob/master/cmd/app-debt-process/main.go#L18-23)

Het app-debt-process component kan gestart worden met behulp van docker:

```bash
docker run -p 8080:8080 \
    -e ORGANIZATION="Demo Organization" \
    -e LINK_REGISTER_ADDRESS="http://demo-organization-link-register" \
    -e LINK_REGISTER_API_KEY="SECRET_LINK_REGISTER_API_KEY" \
    -e DEBT_REGISTER_ADDRESS="http://demo-organization-debt-register" \
    -e DEBT_REGISTER_API_KEY="SECRET_DEBT_REGISTER_API_KEY" \
    registry.gitlab.com/commonground/blauwe-knop/app-debt-process
```

Toelichting op de configuratie:

| Parameter             | Toelichting                                                                                                                   |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| ORGANIZATION          | Moet ingesteld staan op de naam van de bronorganisatie                                                                        |
| LINK_REGISTER_ADDRESS | Moet ingesteld staan op het interne adres waar het link-register van de bronorganisatie bereikbaar is.                        |
| LINK_REGISTER_API_KEY | Moet ingesteld staan op de API key waarmee app-debt-process toegang kan krijgen tot het link-register van de bronorganisatie. |
| DEBT_REGISTER_ADDRESS | Moet ingesteld staan op het interne adres waar het debt-register van de bronorganisatie bereikbaar is.                        |
| DEBT_REGISTER_API_KEY | Moet ingesteld staan op de API key waarmee app-debt-process toegang kan krijgen tot het debt-register van de bronorganisatie. |

Een voorbeeld helm chart is beschikbaar op <https://gitlab.com/commonground/blauwe-knop/app-debt-process/-/tree/master/chart/app-debt-process>

<!-- Het app-debt-process component moet de volgende endpoints publiek beschikbaar stellen:
GET `http://ORGANIZATION_API_BASE_URL/api/v1/schuldenoverzicht`
Zie: https://gitlab.com/commonground/blauwe-knop/app-debt-process/-/blob/master/http/router.go
-->

#### Session Process component

Het session-process kan geconfigureerd worden met een aantal [environment variables](https://gitlab.com/commonground/blauwe-knop/session-process/-/blob/master/cmd/session-process/main.go#L18-21)

Het session-process component kan gestart worden met behulp van docker:

```bash
docker run -p 8087:8087 \
    -e SESSION_REGISTER_ADDRESS="http://demo-organization-session-register" \
    -e SESSION_REGISTER_API_KEY="SECRET_SESSION_REGISTER_API_KEY" \
    registry.gitlab.com/commonground/blauwe-knop/session-process
```

Toelichting op de configuratie:

| Parameter                | Toelichting                                                                                                                     |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------- |
| SESSION_REGISTER_ADDRESS | Moet ingesteld staan op het interne adres waar het session-register van de bronorganisatie bereikbaar is.                       |
| SESSION_REGISTER_API_KEY | Moet ingesteld staan op de API key waarmee session-process toegang kan krijgen tot het session-register van de bronorganisatie. |

Een voorbeeld helm chart is beschikbaar op <https://gitlab.com/commonground/blauwe-knop/session-process/-/tree/master/chart/session-process>

<!-- Het session-process component moet de volgende endpoints publiek beschikbaar stellen:
POST http://ORGANIZATION_SESSION_PROCESS_URL/sessions/startchallenge
POST http://ORGANIZATION_SESSION_PROCESS_URL/sessions/completechallenge
Zie: https://gitlab.com/commonground/blauwe-knop/session-process/-/blob/master/http/router.go
-->

#### App Link Process component

Het app-link-process kan geconfigureerd worden met een aantal [environment variables](https://gitlab.com/commonground/blauwe-knop/app-link-process/-/blob/master/cmd/app-link-process/main.go#L18-28)

Het app-link-process component kan gestart worden met behulp van docker:

```bash
docker run -p 8089:8089 \
    -e ORGANIZATION="Demo Organization" \
    -e ORGANIZATION_OIN="00000000000000000001" \
    -e OUTWAY_ADDRESS="http://demo-organization-nlx-outway" \
    -e LINK_REGISTER_ADDRESS="http://demo-organization-link-register" \
    -e LINK_REGISTER_API_KEY="SECRET_LINK_REGISTER_API_KEY" \
    -e SESSION_REGISTER_ADDRESS="http://demo-organization-session-register" \
    -e SESSION_REGISTER_API_KEY="SECRET_SESSION_REGISTER_API_KEY" \
    registry.gitlab.com/commonground/blauwe-knop/app-link-process
```

Toelichting op de configuratie:

| Parameter                | Toelichting                                                                                                                      |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------------------- |
| ORGANIZATION             | Moet ingesteld staan op de naam van de bronorganisatie                                                                           |
| ORGANIZATION_OIN         | Moet ingesteld staan op de OIN van de bronorganisatie                                                                            |
| OUTWAY_ADDRESS           | Moet ingesteld staan op het interne adres waar de NLX Outway van de bronorganisatie bereikbaar is.                               |
| LINK_REGISTER_ADDRESS    | Moet ingesteld staan op het interne adres waar het link-register van de bronorganisatie bereikbaar is.                           |
| LINK_REGISTER_API_KEY    | Moet ingesteld staan op de API key waarmee app-link-process toegang kan krijgen tot het link-register van de bronorganisatie.    |
| SESSION_REGISTER_ADDRESS | Moet ingesteld staan op het interne adres waar het session-register van de bronorganisatie bereikbaar is.                        |
| SESSION_REGISTER_API_KEY | Moet ingesteld staan op de API key waarmee app-link-process toegang kan krijgen tot het session-register van de bronorganisatie. |

Een voorbeeld helm chart is beschikbaar op <https://gitlab.com/commonground/blauwe-knop/app-link-process/-/tree/master/chart/app-link-process>

<!-- Het app-link-process component moet de volgende endpoints publiek beschikbaar stellen:
GET http://ORGANIZATION_APP_LINK_PROCESS_URL/auth/request-link-token
Zie: https://gitlab.com/commonground/blauwe-knop/app-link-process/-/blob/master/http/router.go
-->

#### Session Register

Het session-register kan geconfigureerd worden met een aantal [environment variables](https://gitlab.com/commonground/blauwe-knop/session-register/-/blob/master/cmd/session-register/main.go#L18-22)

Het session-register component kan gestart worden met behulp van docker:

```bash
docker run -p 8084:8084 \
    -e REDIS_DSN="redis://:$(REDIS_PASSWORD)@$(REDIS_HOST)/$(REDIS_DATABASE_NUMBER)" \
    -e API_KEY="SECRET_SESSION_REGISTER_API_KEY" \
    registry.gitlab.com/commonground/blauwe-knop/session-register
```

Toelichting op de configuratie:

| Parameter | Toelichting                                                                                                        |
| --------- | ------------------------------------------------------------------------------------------------------------------ |
| REDIS_DSN | Moet ingesteld staan op de Redis Data Source Name voor de opslag van de Sessions Redis Store                       |
| API_KEY   | Moet ingesteld staan op een geheime API key waarmee de andere componenten toegang kunnen krijgen tot het register. |

Een voorbeeld helm chart is beschikbaar op <https://gitlab.com/commonground/blauwe-knop/session-register/-/tree/master/chart/session-register>

<!-- Het link-register component moet enkel intern beschikbaar zijn, en ontsluit de volgende data services:
POST http://INTERNAL_SESSION_REGISTER_URL/sessions/
GET http://INTERNAL_SESSION_REGISTER_URL/sessions/
POST http://INTERNAL_SESSION_REGISTER_URL/sessions/challenge
GET http://INTERNAL_SESSION_REGISTER_URL/sessions/challenge
Zie: https://gitlab.com/commonground/blauwe-knop/link-register/-/blob/master/http/router.go
-->

#### Link Register

Het link-register kan geconfigureerd worden met een aantal [environment variables](https://gitlab.com/commonground/blauwe-knop/link-register/-/blob/master/cmd/link-register/main.go#L18-22)

Het link-register component kan gestart worden met behulp van docker:

```bash
docker run -p 8085:8085 \
    -e REDIS_DSN="redis://:$(REDIS_PASSWORD)@$(REDIS_HOST)/$(REDIS_DATABASE_NUMBER)" \
    -e API_KEY="SECRET_LINK_REGISTER_API_KEY" \
    registry.gitlab.com/commonground/blauwe-knop/link-register
```

Toelichting op de configuratie:

| Parameter | Toelichting                                                                                                        |
| --------- | ------------------------------------------------------------------------------------------------------------------ |
| REDIS_DSN | Moet ingesteld staan op de Redis Data Source Name voor de opslag van de Links Redis Store                          |
| API_KEY   | Moet ingesteld staan op een geheime API key waarmee de andere componenten toegang kunnen krijgen tot het register. |

Een voorbeeld helm chart is beschikbaar op <https://gitlab.com/commonground/blauwe-knop/link-register/-/tree/master/chart/link-register>

<!-- Het link-register component moet enkel intern beschikbaar zijn, en ontsluit de volgende data services:
GET http://INTERNAL_LINK_REGISTER_URL/link-tokens/
POST http://INTERNAL_LINK_REGISTER_URL/link-tokens/
PUT http://INTERNAL_LINK_REGISTER_URL/link-tokens/{linkToken}
GET http://INTERNAL_LINK_REGISTER_URL/link-tokens/{linkToken}
Zie: https://gitlab.com/commonground/blauwe-knop/link-register/-/blob/master/http/router.go
-->

#### Sessions (Redis store)

Dit is een standaard redis installatie.
Met behulp van docker kan Redis bijvoorbeeld gestart worden via:

```bash
docker run -p 6379:6379 redis:6.2.6-alpine
```

#### Link (Redis store)

Dit is een standaard redis installatie.
Met behulp van docker kan Redis bijvoorbeeld gestart worden via:

```bash
docker run -p 6379:6379 redis:6.2.6-alpine
```

### 3. App Debt Process koppelen aan Debt Registers (bronsystemen)

Over het algemeen is schuldinformatie bij een bronorganisatie in één of meerdere (al dan niet bestaande) registers opgeslagen. Een dergelijk bronsysteem heet binnen de Blauwe Knop architectuur een `debt-register`.

In de Blauwe Knop proeftuinomgeving is als voorbeeld een referentie-implementatie van dit component beschikbaar. In de praktijk zullen bronorganisaties deze referentie-component vermoedelijk niet hergebruiken maar de API-specificatie op bestaande bronsystemen implementeren.

De koppeling tussen het app-debt-process en één of meerdere debt-registers wordt gelegd via de `DEBT_REGISTER_ADDRESS` configuratie-instelling van het app-debt-process (zie hierboven), en eventueel beveiligd via de `DEBT_REGISTER_API_KEY` configuratie-instelling.

Bronsystemen die als debt-register gebruikt worden moeten de meest recente versie van de Blauwe Knop API specificatie voor schuldinformatie implementeren. Dit is een OpenAPI3 specificatie, en deze is te vinden op [https://gitlab.com/commonground/blauwe-knop/debt-register/-/blob/master/openapi.yml](https://gitlab.com/commonground/blauwe-knop/debt-register/-/blob/master/openapi.yml)

### 4. Proces-API componenten publiek beschikbaar maken

De componenten moeten publiek beschikbaar zijn, waarbij een deel van de urls zelf geconfigureerd en gekozen kan worden. Het gaat dan om

`ORGANIZATION_API_BASE_URL`
`ORGANIZATION_SESSION_PROCESS_URL`
`ORGANIZATION_APP_LINK_PROCESS_URL`

Deze URL's moeten vervolgens bij de aanmelding in het Blauwe Knop Stelsel worden gecommuniceerd aan de beheerder van het stelse.

### 5. Aanmelden als deelnemer in het Blauwe Knop Stelsel

De beheerder van het stelsel voegt de discovery-informatie van de bronorganisatie toe aan het Blauwe Knop Stelselschema. Het stelselschema wordt bijgehouden op: <https://gitlab.com/commonground/blauwe-knop/scheme/-/blob/master/organizations.json>

## C. Aan de slag als registrator

_Nog niet beschikbaar_

## D. Aan de slag als stelselbeheerder

_Nog niet beschikbaar_
