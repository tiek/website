---
id: processen
title: Processen
sidebar_label: Processen
---

## Procesanalyse

Het belangrijkste onderdeel van het proces om een overzicht van schuldinformatie te verzamelen is natuurlijk het daadwerkelijk ophalen van schuldinformatie bij alle bronorganisaties. Om dat veilig en geautomatiseerd te kunnen doen zijn een aantal andere stappen nodig, waaronder controleren wie de aanvragende persoon is (zodat gegevens enkel worden verstrekt aan de juiste persoon), en het leggen van een veilige, herhaaldelijk geautomatiseerd bruikbare koppeling ('link') tussen de telefoon van de burger en de bronorganisaties.

### Drie varianten

Voor de volgorde van deze processen hebben we drie varianten onderzocht:

  1. De variant waarbij de burger bij iedere bronorganisatie apart inlogt en zijn identiteit bewijst
  2. De variant waarbij de burger één organisatie kiest die optreedt als 'registrator', waar de burger éénmalig inlogt en een generiek verzoek tot het ophalen van schuldinformatie neerlegt. Andere bronorganisaties kunnen dan op deze vastlegging vertrouwen.
  3. De variant waarbij de burger een generiek verzoek tot het ophalen van schuldinformatie ondertekent met zijn digitale identiteit, waardoor geen registrator meer nodig is.

### Focus op variant 2: De 'Blauwe Knop registrator'

Omdat variant 2 kan worden uitgevoerd met Digid, waarover veel overheidsorganisaties reeds beschikken, hebben we met name variant 2 verder uitgewerkt. Variant 3 kan pas op grotere schaal werken als burgers beschikken over een veilige digitale identiteit die bij meerdere organisaties bruikbaar is.

Het globale proces van variant 2 is als volgt:

![BPMN procesplaat variant 2](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/bpmn-totaal-variant-2.png)

### BPMN processen voor variant 2

Generiek verzoek samenstellen:

![BPMN generiek verzoek samenstellen](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/bpmn-generiek-verzoek-samenstellen.png)

Generiek verzoek indienen:

![BPMN generiek verzoek indienen](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/bpmn-generiek-verzoek-indienen.png)

Generiek verzoek ontvangen:

![BPMN generiek verzoek ontvangen](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/bpmn-generiek-verzoek-ontvangen.png)

